// Core
const path = require('path')
const dotenv = require('dotenv')

// Third Party Node Packages
const express = require('express')
const favicon = require('serve-favicon')
const logger = require('morgan')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy

// Custom
const adminRoutes = require('./routes/admin/admin')
const apiRoutes = require('./routes/admin/blog/blog')
const mainRoutes = require('./routes/main')
const errorRoutes = require('./routes/error/error')
const errorController = require('./controllers/error')

const discordBot = require('./controllers/discord-bot')

const app = express()

dotenv.config()

app.set('view engine', 'ejs')
app.set('views', 'views')

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(
  require('express-session')({
    secret: process.env.EXPRESS_SESSION_SECRET,
    resave: false,
    saveUninitialized: false
  })
)
app.use(passport.initialize())
app.use(passport.session())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/admin', adminRoutes)
app.use('/api', apiRoutes)
app.use(mainRoutes)
app.use('/error', errorRoutes)

const Account = require('./models/account')
passport.use(new LocalStrategy(Account.authenticate()))
passport.serializeUser(Account.serializeUser())
passport.deserializeUser(Account.deserializeUser())

app.use(errorController.get404)

mongoose
  .connect(
    process.env.MONGOLAB_URI
  )
  .then(result => {
    app.listen(3001)
  })
  .catch(err => {
    console.log(err)
  })

module.exports = app
