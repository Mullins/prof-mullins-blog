/* eslint-disable */
/* global describe it */
const assert = require('assert')
const chai = require('chai')
const expect = require('chai').expect
const should = require('should')
const mainController = require('../controllers/main')
const adminController = require('../controllers/admin')

chai.use(require('chai-http'))

const app = require('../index.js')

let request = {}
let response = {
  viewName: '',
  data: {},
  render: function(view, viewData) {
    this.viewName = view
    this.data = viewData
  }
}

describe('routing', function() {
  this.timeout(1000)
  
  describe('default route', function() {
    it('should provide the title and view name', function() {
      mainController.getIndex(request, response)
      response.viewName.should.equal('index')
    })
  })

  describe('about route', function() {
    it('should provide the title and view name', function() {
      mainController.getAbout(request, response)
      response.viewName.should.equal('about')
    })
  })

  describe('contact route', function() {
    it('should provide the title and view name', function() {
      mainController.getContact(request, response)
      response.viewName.should.equal('contact')
    })
  })

  describe('register route', function() {
    it('should provide the title and view name', function() {
      mainController.getRegister(request, response)
      response.viewName.should.equal('register')
    })
  })

  describe('login route', function() {
    it('should provide the title and view name', function() {
      mainController.getLogin(request, response)
      response.viewName.should.equal('login')
    })
  })

  describe('userAccount route', function() {
    it('should provide the title and view name', function() {
      mainController.getUserAccount(request, response)
      response.viewName.should.equal('user-account')
    })
  })

  describe('tools route', function() {
    it('should provide the title and view name', function() {
      mainController.getTools(request, response)
      response.viewName.should.equal('tools')
    })
  })

  describe('ping route', function() {
    it('should provide pong', function() {
      return chai
        .request(app)
        .get('/ping')
        .then(res => {
          expect(res).to.have.status(200)
          expect(res).to.be.an('object')
          expect(res.body).to.be.an('object')
          expect(res.text).to.be.equal('pong!')
        })
    })
  })
})
