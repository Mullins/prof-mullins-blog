/* eslint-disable */
/* global describe it */
const assert = require('assert')
const chai = require('chai')
const expect = require('chai').expect
const should = require('should')
const adminController = require('../controllers/admin')

chai.use(require('chai-http'))

const app = require('../index.js')

let request = {}
let response = {
  viewName: '',
  data: {},
  render: function(view, viewData) {
    this.viewName = view
    this.data = viewData
  }
}

describe('admin routing', function() {
  this.timeout(1000)

  describe('admin default route', function() {
    it('should provide the title and view name for default admin route', function() {
      adminController.getAdminIndex(request, response)
      response.viewName.should.equal('admin/admin')
    })
  })

  describe('admin add-blog route', function() {
    it('should provide the title and view name for add-blog', function() {
      adminController.getAddBlog(request, response)
      response.viewName.should.equal('admin/add-blog')
    })
  })
})
