/* eslint-disable */
/* global describe it */
const assert = require('assert')
const chai = require('chai')
const expect = require('chai').expect
const should = require('should')
const mainController = require('../controllers/main')
const adminController = require('../controllers/admin')

chai.use(require('chai-http'))

const app = require('../index.js')

describe('API endpoint /api/', function() {
  this.timeout(1000)

  it('should return all blogs', function() {
    return chai
      .request(app)
      .get('/api/blogs')
      .then(res => {
        expect(res).to.have.status(200)
        expect(res).to.be.an('object')
        expect(res.body).to.be.an('array')
      })
  })

  it('should return a single blog', function() {
    return chai
      .request(app)
      .get('/api/blog/5bf1a8fb6f30143c55fc685c')
      .then(res => {
        expect(res).to.have.status(200)
        expect(res).to.be.an('object')
        expect(res.body).to.be.an('object')
      })
  })
})
