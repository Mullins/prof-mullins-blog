const mongoose = require('mongoose')

const Schema = mongoose.Schema

const blogSchema = new Schema({
  author: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    required: true,
    default: Date.now
  },
  body: {
    type: String,
    required: true
  },
  draft: {
    type: Boolean,
    default: false
  },
  imageUrl: {
    type: String,
    default: '/images/computer_geek.png'
  },
  comment: [
    {
      body: String,
      date: Date
    }
  ]
})

module.exports = mongoose.model('Blog', blogSchema)
