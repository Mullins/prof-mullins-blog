# Professor Mullins Blog

This project will serve as my personal blog.  I am a software developer, leader, and college professor.  This finished project will be hosted on one of my domains.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them
* Git
  * Linux  
    ```bash
    # Apt
    sudo apt-get install git
    # Pacman
    sudo pacman -S git
    # Yum
    sudo yum install git
    # urpm
    sudo urpmi git
    # dnf
    sudo dnf install git
    ```
  * Mac via Homebrew
    ```bash
    # If you do not have Homebrew installed, install with the below
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" brew doctor
    # Now install Git
    brew install git
    ```
  * Windows
    * Download from [here](https://gitforwindows.org/) and then install
* NodeJS and NPM
  * Linux
    ```bash
    # Apt
    sudo apt-get install nodejs npm
    # Pacman
    sudo pacman -S nodejs npm
    # Yum (need EPEL repository first)
    sudo yum install epel-release
    sudo yum install nodejs npm
    # dnf
    sudo dnf install nodejs npm
    ```
  * Mac (via Homebrew)
    ```bash
    brew install node
    ```
  * Windows
    * Download the installer [here](https://nodejs.org/en/#download) and then install 


### Installing

1. Determine where you want to save it and then clone the repo
    ```bash
    git clone git@gitlab.com:Mullins/prof-mullins-blog.git
    ```
2. Install
    ```bash
    npm install
    ```
3. Create a **FREE** mongoDB Atlas account [here](https://www.mongodb.com/cloud/atlas)
4. Once you have created a cluster on mongoDB Atlas you will need to add users
    * Click the security tab and click 'ADD NEW USER'
        * New user will need at least read and write access to any database
        * Provide a username and password (save these values, you will need them)
    * Now back to the Overview tab
        * Click 'CONNECT' and then choose 'Connect Your Application'
            * Click 'SRV connection string (3.6+ driver) and copy the SRV address
5. Now that you have a mongoDB cluster you will need to store your connection string to be used in the application
    * Create a .env file at the root of your project
        * Add the connection string to the .env file, similiar to the following, replacing `<username>` and `<password>` with your username and password
          ```bash
          MONGOLAB_URI="mongodb+srv://<username>:<password>@cluster0-rbs0v.mongodb.net/test?retryWrites=true"
          ```
6. Now make sure the server starts by running the following.
    ```bash
    npm start
    ```


## Running the tests

**TODO:** Create more tests and update the test script
```bash
npm test
```

### And coding style tests

The project uses eslint as a linter.

## Built With

* [mongoDB Atlas](https://www.mongodb.com/cloud/atlas) - The cloud mongoDB database
* [NodeJS](https://nodejs.org/en/)
* [Express](https://expressjs.com/) - The views templating engine
* [mongoose](https://mongoosejs.com/) - mongodb object modeling
* [nodemon](https://nodemon.io/)
* and many other tools and packages

## Authors

* **Nicholas Mullins**
    * [Mullins.io](https://mullins.io)
    * [NicholasMullins.com](https://nicholasmullins.com/)
    * [ProfessorMullins.io](https://www.professormullins.io/)
    * [ProfessorMullins.com](https://professormullins.com/)
    * [Twitter - ProfMullins_CIS](https://twitter.com/ProfMullins_CIS)
    * [LinkedIn](https://www.linkedin.com/in/nicholas-mullins-5ba48341/)

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Coming Soon
