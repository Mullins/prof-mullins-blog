// Third Party Modules
const passport = require('passport')
const Account = require('../models/account')
const { validationResult } = require('express-validator/check')

exports.getIndex = (req, res, next) => {
  let username = ''
  if (req.session && req.session.passport && req.session.passport.user) {
    username = req.session.passport.user
  }

  res.render('index', { pageTitle: 'Home', path: '/', username: username })
}

exports.getAbout = (req, res, next) => {
  let username = ''
  if (req.session && req.session.passport && req.session.passport.user) {
    username = req.session.passport.user
  }

  res.render('about', {
    pageTitle: 'About',
    path: '/about',
    username: username
  })
}

exports.getContact = (req, res, next) => {
  let username = ''
  if (req.session && req.session.passport && req.session.passport.user) {
    username = req.session.passport.user
  }

  res.render('contact', {
    pageTitle: 'Contact',
    path: '/contact',
    username: username
  })
}

exports.getRegister = (req, res, next) => {
  let username = ''
  if (req.session && req.session.passport && req.session.passport.user) {
    username = req.session.passport.user
  }

  res.render('register', {
    pageTitle: 'Register',
    path: '/register',
    username: username
  })
}

exports.postRegister = (req, res, next) => {
  const errors = validationResult(req)
  let username = ''
  if (!errors.isEmpty()) {
    let allErrors = errors.mapped()
    res.render('register', {
      pageTitle: 'register',
      path: '/register',
      username: username,
      errors: allErrors
    })
  } else {
    Account.register(
      new Account({ username: req.body.username, email: req.body.email }),
      req.body.password,
      (err, account) => {
        if (err) {
          return res.render('register', {
            account: account,
            pageTitle: 'Register',
            path: '/register'
          })
        }
        passport.authenticate('local')(req, res, () => {
          res.redirect('/')
        })
      }
    )
  }
}

exports.getLogin = (req, res, next) => {
  let username = ''
  if (req.session && req.session.passport && req.session.passport.user) {
    username = req.session.passport.user
  }

  res.render('login', {
    user: req.user,
    pageTitle: 'Login',
    path: '/login',
    username: username
  })
}

exports.postLogin = (req, res, next) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    let allErrors = errors.mapped()
    res.render('login', {
      pageTitle: 'login',
      path: '/login',
      username: '',
      errors: allErrors
    })
  } else {
    res.redirect('/')
  }
}

exports.getLogout = (req, res, next) => {
  req.logout()
  res.redirect('/')
}

exports.getUserAccount = (req, res, next) => {
  let username = ''
  if (req.session && req.session.passport && req.session.passport.user) {
    username = req.session.passport.user
  }

  res.render('user-account', {
    pageTitle: 'Account',
    path: '/user-account',
    username: username
  })
}

exports.getTools = (req, res, next) => {
  res.render('tools', { pageTitle: 'Tools', path: '/tools', username: '' })
}

exports.getPing = (req, res, next) => {
  res.status(200).send('pong!')
}
