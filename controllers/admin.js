// Third Party Modules
const { validationResult } = require('express-validator/check')

// Custom modules
const Blog = require('../models/blog')

exports.getAdminIndex = (req, res, next) => {
  let username = ''

  res.render('admin/admin', {
    pageTitle: 'Admin',
    path: '/admin/admin',
    username: username
  })
}

exports.getAddBlog = (req, res, next) => {
  let username = ''
  res.render('admin/add-blog', {
    pageTitle: 'Add blog',
    path: '/admin/add-blog',
    username: username
  })
}

exports.postAddBlog = (req, res, next) => {
  let username = ''
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    let allErrors = errors.mapped()
    res.render('admin/add-blog', {
      pageTitle: 'Add Blog',
      path: '/admin/add-blog',
      username: username,
      errors: allErrors
    })
  } else {
    const blogAuthor = req.body.blogAuthor
    const blogTitle = req.body.blogTitle
    const blogDate = req.body.blogDate
    const blogBody = req.body.blogBody
    const blogDraft = req.body.blogDraft === '1'
    const blog = new Blog({
      author: blogAuthor,
      title: blogTitle,
      date: blogDate,
      body: blogBody,
      draft: blogDraft
    })

    blog
      .save()
      .then(result => {
        res.redirect('/admin/blog-list')
      })
      .catch(err => {
        console.log(err)
      })
  }
}

exports.getBlogList = (req, res, next) => {
  let username = ''

  Blog.find()
    .sort({ date: -1 })
    .then(blogs => {
      res.render('admin/blog-list', {
        pageTitle: 'Blog List',
        path: '/admin/blog-list',
        username: username,
        blogs: blogs
      })
    })
    .catch(err => {
      console.log(err)
    })
}

exports.getEntryById = (req, res, next) => {
  let username = ''
  const blogId = req.params.id
  Blog.findById(blogId)
    .sort({ date: -1 })
    .then(blog => {
      res.render('admin/entry', {
        pageTitle: 'Blog List',
        path: '/admin/entry',
        username: username,
        blog: blog
      })
    })
    .catch(err => {
      console.log(err)
    })
}
