const fs = require('fs')

const Discord = require('discord.js')
const client = new Discord.Client()

const dotenv = require('dotenv')

const commands = JSON.parse(fs.readFileSync('./data/bot-commands.json', 'utf8'))

dotenv.config()

client.login(process.env.DISCORD_TOKEN)

client.on('ready', () => {
  console.log('Bot has started')
  client.user.setActivity('Serving ' + client.guilds.size + ' servers')
})

client.on('message', message => {
  if (message.author.bot) return

  if (message.content.charAt(0) === '!') {
    let botCommand = message.content.substr(1)

    if (typeof commands[botCommand] !== 'undefined') {
      if (botCommand === 'coin toss') {
        let coin = Math.floor(Math.random() * 2 + 1)

        // Send initial message
        message.channel.send(commands[botCommand]).catch(err => {
          console.log(err)
        })

        // Wait 4 seconds before saying if heads or tails
        setTimeout(() => {
          message.channel.send(coin === 1 ? 'heads' : 'tails').catch(err => {
            console.log(err)
          })
        }, 4000)
      } else {
        message.channel.send(commands[botCommand]).catch(err => {
          console.log(err)
        })
      }
    }
  }
})

module.exports = client
