const Blog = require('../../models/blog')

exports.getBlogList = (req, res, next) => {
  Blog.find()
    .sort({ date: -1 })
    .then(blogs => {
      res.status(200).send(blogs)
    })
    .catch(err => {
      console.log(err)
    })
}

exports.getEntryById = (req, res, next) => {
  const blogId = req.params.id
  Blog.findById(blogId)
    .sort({ date: -1 })
    .then(blog => {
      res.status(200).send(blog)
    })
    .catch(err => {
      console.log(err)
    })
}
