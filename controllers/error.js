exports.get404 = (req, res, next) => {
  let username = ''
  if (req.session.passport && req.session.passport.user) {
    username = req.session.passport.user
  }

  res.status(404).render('error/404', {
    pageTitle: 'Page Not Found',
    path: '/404',
    username: username
  })
}

exports.get401 = (req, res, next) => {
  let username = ''
  if (req.session.passport && req.session.passport.user) {
    username = req.session.passport.user
  }

  res.status(401).render('error/401', {
    pageTitle: 'Username not found.',
    path: '/401',
    username: username
  })
}
