const { check } = require('express-validator/check')

exports.registrationValidation = [
  check('email')
    .isEmail()
    .withMessage('Must be a valid email address.')
    .exists()
    .withMessage('Email address is required.'),
  check('password')
    .exists()
    .withMessage('Password is required.')
    .isLength({ min: 8, max: 32 })
    .withMessage('Password must be between 8 and 32 characters')
    .matches(/\d/)
    .withMessage('Password must contain a number.')
    .matches(/[a-z]/)
    .withMessage('Password must contain a lower case letter')
    .matches(/[A-Z]/)
    .withMessage('Password must contain a capital letter.')
    .matches(/[?|!|@|#|$|%|^|&|*]/)
    .withMessage(
      'Password must contain one of the following special characters: ?, !, @, #, $, %, &, *'
    ),
  check('username')
    .exists()
    .withMessage('Username is required.')
    .isString()
    .withMessage('Username cannot contain only numbers.')
    .isLength({ min: 6, max: 32 })
    .withMessage('Username must be between 6 and 32 characters in length.')
]

exports.loginValidation = [
  check('username')
    .exists()
    .withMessage('Username is required.'),
  check('password')
    .exists()
    .withMessage('Password is required.')
]

exports.blogValidation = [
  check('blogAuthor')
    .exists()
    .withMessage('Author is required.')
    .isString()
    .withMessage('Author must not be all numeric.')
    .isLength({ min: 3, max: 50 })
    .withMessage('Author must be between 3 and 50 characters.'),
  check('blogTitle')
    .exists()
    .withMessage('Title is required.')
    .isString()
    .withMessage('Title must be a string')
    .isLength({ min: 3, max: 100 })
    .withMessage('Title must be between 3 and 50 characters.'),
  check('blogBody')
    .exists()
    .withMessage('Body is required.')
    .isLength({ min: 20, max: 5000 })
    .withMessage('Incorrect size of blog body.')
]
