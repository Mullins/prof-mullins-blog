// Third Party Modules
const express = require('express')

// Custom Modules
const blogApiController = require('../../../controllers/api/blog')

const router = express.Router()

router.get('/blogs', blogApiController.getBlogList)

router.get('/blog/:id', blogApiController.getEntryById)

module.exports = router
