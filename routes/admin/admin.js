// Third Party Modules
const express = require('express')

// Custom modules
const validator = require('../../util/validate')
const adminController = require('../../controllers/admin')

const router = express.Router()

router.get('/', adminController.getAdminIndex)

router.get('/add-blog', adminController.getAddBlog)

router.post('/add-blog', validator.blogValidation, adminController.postAddBlog)

router.get('/blog-list', adminController.getBlogList)

router.get('/entry/:id', adminController.getEntryById)

module.exports = router
