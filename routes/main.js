// Third Party Modules
const express = require('express')
const passport = require('passport')

// Custom modules
const validator = require('../util/validate')
const mainController = require('../controllers/main')

const router = express.Router()

router.get('/', mainController.getIndex)

router.get('/about', mainController.getAbout)

router.get('/contact', mainController.getContact)

router.get('/register', mainController.getRegister)

router.post(
  '/register',
  validator.registrationValidation,
  mainController.postRegister
)

router.get('/login', mainController.getLogin)

router.post(
  '/login',
  validator.loginValidation,
  passport.authenticate('local', { failureRedirect: '/error/401' }),
  mainController.postLogin
)

router.get('/logout', mainController.getLogout)

router.get('/user-account', mainController.getUserAccount)

router.get('/tools', mainController.getTools)

router.get('/ping', mainController.getPing)

module.exports = router
