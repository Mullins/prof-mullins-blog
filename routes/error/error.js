// Third Party Modules
const express = require('express')

const router = express.Router()

router.get('/404', (req, res, next) => {
  let username = ''
  if (req.session.passport && req.session.passport.user) {
    username = req.session.passport.user
  }

  res.status(404).render('error/404', {
    pageTitle: 'Page Not Found',
    path: '/404',
    username: username
  })
})

router.get('/401', (req, res, next) => {
  let username = ''
  if (req.session.passport && req.session.passport.user) {
    username = req.session.passport.user
  }

  res.status(401).render('error/401', {
    pageTitle: 'Page Not Found',
    path: '/401',
    username: username
  })
})

module.exports = router
